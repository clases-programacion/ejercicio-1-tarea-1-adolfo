import javax.swing.JOptionPane;

public class Ejercicio1 {
  public static void main(String[] args) {
    //Declaramos las unidades
    String [] unidades = {
      "", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve"
    };
    //Declaramos las decenas
    String [] decenas = {
      "", "diez", "veinte", "treinta", "cuarenta", "cincuenta", "sesenta", "setenta", "ochenta", "noventa"
    };
    //Declaramos las centenas
    String [] centenas = {
      "", "ciento", "doscientos", "trescientos", "cuatrocientos", "quinientos", "seiscientos", "setecientos", "ochocientos", "novecientos"
    };
    String numeroTercios = ".";
    String numeroLetra = ".";
    try {
      //Capturamos el número que el usuario ingresó
      int nEntero = Integer.parseInt(JOptionPane.showInputDialog("Ingresa un numero entero menor o igual a 2,147,483,647"));
      //Transformamos el número a array de caracteres
      char[] numero = Integer.toString(nEntero).toCharArray();
      int pasos = 0;
      boolean printMil = false;
      boolean printMillon = false;
      //Hacemos un for de las unidades a los millones
      for (int i = numero.length - 1; i >= 0; i--) {
        //contamos cuantas veces hemos pasado
        pasos++;
        /**
         * En caso de estar en los pasos 4 al 6 estamos en los miles
         * Verificamos si ya pintamos mil
         * Verificamos que el valor del caracter no sea 0
         */
        if (pasos >= 4 && pasos <= 6 && printMil == false && !unidades[Character.getNumericValue(numero[i])].equals("")) {
          printMil = true;
          numeroTercios = "mil " + numeroTercios;
        }
        /**
         * En caso de estar en pasos mayores a 7 estamos en los millones
         * Verificamos si ya pintamos millones
         * Verificamos que el valor del caracter no sea 0
         */
        else if (pasos >= 7 && printMillon == false && !unidades[Character.getNumericValue(numero[i])].equals("")) {
          numeroTercios = "millones " + numeroTercios;
          printMillon = true;
        }
        switch (pasos % 3) {
          case 1:
            //Verificamos que el valor del caracter no sea 0
            if (unidades[Character.getNumericValue(numero[i])].equals("")) {
              break;
            }
            numeroTercios = unidades[Character.getNumericValue(numero[i])] + " " + numeroTercios;
            break;
          case 2:
            //Verificamos que el valor del caracter no sea 0
            if (!decenas[Character.getNumericValue(numero[i])].equals("")) {
              numeroTercios = decenas[Character.getNumericValue(numero[i])] + " y " + numeroTercios;
            }
            break;
          case 0:
            //Verificamos que el valor del caracter no sea 0
            if (!centenas[Character.getNumericValue(numero[i])].equals("")) {
              numeroTercios = centenas[Character.getNumericValue(numero[i])] + " " + numeroTercios;
            }
            break;
        }
        //Ajustamos los textos
        if (numeroTercios.equals("uno mil ")) {
          numeroTercios = "mil ";
        } else if (numeroTercios.equals("uno millones ")) {
          numeroTercios = "un millon ";
        }
        numeroLetra = numeroTercios + numeroLetra;
        numeroTercios = "";
      }
    } catch(Exception e) {
      //Algo tronó
      System.out.println("Joder!!!! Dije número menor o igual a 2,147,483,647");
    }
    //Ajustamos los textos e imprimimos
    System.out.println(
      numeroLetra.replace("diez y uno", "once").replace("diez y dos", "doce").replace("diez y tres", "trece").replace("diez y cuatro", "catorce").replace("diez y cinco", "quince")
        .replace("  y ", " y ").replace(" .", "").replace("ciento  ", "cien ").replace("y  mil", "mil").replace(" y  ", "").replace("y uno ", "y un ").replace(" y.", "")
        .replace(" y millones", " millones").replace(" y mil", " mil").replace("ciento mil", "cien mil"));
  }
}
